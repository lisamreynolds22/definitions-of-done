

export class Lane {
  public id: string;
  public title: string;
  public sections: Section[] = [];
  
  constructor(id: string, title: string, sections: Section[]) {
    this.id = id;
    this.title = title;
    this.sections = sections;
  }

}

export class Section {
  public title: string;
  public tasks: Task[] = [];
  
  constructor(title: string, tasks: Task[]) {
    this.title = title;
    this.tasks = tasks;
  }
  
}

export class Task {
  public title: string;
  public complete: boolean = false;
  
  constructor(title: string) {
    this.title = title;
  }
  
}