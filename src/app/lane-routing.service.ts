import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Lane } from './lane';

@Injectable()
export class LaneRoutingService {
  laneChange$ = new BehaviorSubject<Lane>(null);
}
