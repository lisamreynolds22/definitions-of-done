import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TaskComponent } from './task/task.component';
import { LaneComponent } from './lane/lane.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule,
         MatButtonModule, 
         MatCheckboxModule,
         MatIconModule,
         MatFormFieldModule,
         MatInputModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { NavComponent } from './nav/nav.component';

import { LaneService } from './lane.service';
import { LaneRoutingService } from './lane-routing.service';

@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    LaneComponent,
    SearchBarComponent,
    NavComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    AppRoutingModule,
  ],
  providers: [
    LaneService, 
    LaneRoutingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
