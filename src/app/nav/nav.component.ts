import { Component } from '@angular/core';
import { Router, RouterOutlet, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, share, delay, startWith, pairwise } from 'rxjs/operators';
import { trigger, style, animate, transition, group, query } from '@angular/animations';
import { LaneService } from '../lane.service';
import { LaneRoutingService } from '../lane-routing.service';
import { Lane } from '../lane';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
  animations: [
    trigger('routeSlide', [
      transition('* <=> *', [
        group([
          query(':enter', [
            style({transform: 'translateX({{offsetEnter}}%)'}),
            animate('0.3s ease-in-out', style({transform: 'translateX(0%)'}))
          ], {optional: true}),
          query(':leave', [
            style({transform: 'translateX(0%)', position: 'absolute', top: '0'}),
            animate('0.3s ease-in-out', style({transform: 'translateX({{offsetLeave}}%)'}))
          ], {optional: true}),
        ])
      ]),
    ])
  ]
})
export class NavComponent {
  laneChange$: Observable<Lane>;
  prev$: Observable<Lane>;
  next$: Observable<Lane>;
  routeTrigger$: Observable<Object>;

  constructor(private laneService: LaneService,
              private laneRoutingService: LaneRoutingService,
              private router: Router) {
    this.laneChange$ = this.laneRoutingService.laneChange$
    
    this.prev$ = this.laneChange$
      .pipe(
        delay(0),
        map(lane => lane != undefined ? this.laneService.getPreviousId(lane.id) : null),
        share()
      );
    this.next$ = this.laneChange$
      .pipe(
        delay(0),
        map(lane => lane != undefined ? this.laneService.getNextId(lane.id) : null),
        share()
      );
       
    this.routeTrigger$ = this.laneChange$
      .pipe(
        startWith(null),
        pairwise(),
        map(([prev, curr]) => ({
          value: curr,
          params: {
            offsetEnter: this.laneService.getIndex(prev) > this.laneService.getIndex(curr) ? -100 : 100,
            offsetLeave: this.laneService.getIndex(prev) > this.laneService.getIndex(curr) ? 100 : -100
          }
        })),
      );
  }
  
}
