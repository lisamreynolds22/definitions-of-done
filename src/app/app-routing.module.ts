import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router, RouteReuseStrategy } from '@angular/router';
import { CustomReuseStrategy } from './CustomReuseStrategy';
import { LaneComponent } from './lane/lane.component';
import { NavComponent } from './nav/nav.component';

const routes: Routes = [
  { path: '', redirectTo: 'lane', pathMatch: 'full' },
  { path: 'lane', component: NavComponent,
    children: [
      { path: '', redirectTo: 'to-do', pathMatch: 'full' },
      { path: ':id', component: LaneComponent }
    ]
  },
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    RouterModule.forChild(routes)
  ],
  providers: [{
    provide: RouteReuseStrategy,
    useClass: CustomReuseStrategy
  }],
  exports: [ RouterModule ]
})
export class AppRoutingModule { } 