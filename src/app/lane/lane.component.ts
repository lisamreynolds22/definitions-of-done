import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LaneService } from '../lane.service';
import { LaneRoutingService } from '../lane-routing.service';
import { Lane } from '../lane';

@Component({
  selector: 'app-lane',
  templateUrl: './lane.component.html',
  styleUrls: ['./lane.component.css']
})
export class LaneComponent implements OnInit {
  lane: Lane;

  constructor(private route: ActivatedRoute, private laneService: LaneService, private laneRoutingService: LaneRoutingService) { }

  ngOnInit() {
    const { id } = this.route.snapshot.params;
    this.lane = this.laneService.get(id);
    
    this.laneRoutingService.laneChange$.next(this.lane);
  }

}
