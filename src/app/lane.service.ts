import { Injectable } from '@angular/core';
import { Lane, Section, Task } from './lane';

@Injectable()
export class LaneService {

  lanes: Lane[] = []
  
  constructor() {
    // this.createTestLanes();
    this.lanes.push(this.createToDoLane());
    this.lanes.push(this.createInProgressLane());
    this.lanes.push(this.createCodeReviewLane());
  }
  
  createTestLanes() {
    let cook = new Section("Make scrambled eggs", [
      new Task("Crack some eggs"),
      new Task("Add salt and pepper"),
      new Task("Mix in some peppers and salami"),
      new Task("Cook it all"),
    ]);
    
    let pet = new Section("Pet a cat", [
      new Task("Find a cat"),
      new Task("Pet that cat"),
    ]);
    
    let pack = new Section("Pack for a trip", [
      new Task("Clothes"),
      new Task("Underwear"),
      new Task("Pajamas"),
      new Task("Headphones"),
      new Task("Chargers"),
      new Task("Shoes"),
      new Task("Book"),
    ]);
    
    let todo = new Lane('to-do', 'To Do', [pet, pack]);
    this.lanes.push(todo);
    
    let inprogress = new Lane('in-progress', 'In Progress', [cook]);
    this.lanes.push(inprogress);
    
    let codereview = new Lane('code-review', 'Code Review', [cook, pet, pack]);
    this.lanes.push(codereview);
  }
  
  createToDoLane(): Lane {
    let readyToStart = new Section("Ready to Start?", [
      new Task("You know what the Jira issue is asking for"),
      new Task("You know how you will do it"),
      new Task("You've asked for clarity on anything missing from above"),
      new Task("You don't have any other items <span class='color-in-progress'>In Progress</span>"),
    ]);
    
    return new Lane('to-do', 'To Do', [readyToStart]);
  }
  
  createInProgressLane(): Lane {
    let acceptanceCriteria = new Section("Acceptance criteria", [
      new Task("Have been fully met, without workarounds"),
    ]);
    
    let testCases = new Section("Test cases", [
    new Task("New test cases written, or documented explanation of why they weren't"),
    new Task("Existing test cases pass for all <em>possibly related</em> projects (ask if unsure)"),
    new Task("If using a feature branch, full test suite on Jenkins is run, and all tests pass"),
    ]);
    
    let javadoc = new Section("Javadoc comments", [
    new Task("On all public methods (besides get and set) describing how, why, and when to use"),
    new Task("On all hacks/delicate code, explaining why it had to be done that way"),
    ]);
    
    return new Lane('in-progress', 'In Progress', [acceptanceCriteria, testCases, javadoc]);
  }
  
  createCodeReviewLane(): Lane {
    let meetInPerson = new Section("Meet with dev in person (unless trivial)", [
      new Task("Dev runs you through the code"),
      new Task("Ask any questions for clarity"),
      new Task("Share pointers/coding strategies"),
    ]);
    
    let inProgressMet = new Section("These <span class='color-in-progress'>In Progress</span> definitions are met, including all sub-bullet points", [
      new Task("Acceptance criteria"),
      new Task("Test cases"),
      new Task("Javadoc comments"),
      new Task("Comments on Jira issue"),
      new Task("Jira fields filled out"),
      new Task("Thorough documentation"),
    ]);
    
    let other = new Section("Other things to look for", [
      new Task("Code is clear and well organized"),
      new Task("Built with testability in mind"),
      new Task("Uses <em>Single Responsibility Principle</em>"),
      new Task("Follows UI/UX standards"),
      new Task("Has no obvious edge cases"),
    ]);
    
    return new Lane('code-review', 'Code Review', [meetInPerson, inProgressMet, other]);
  }
  
  get(id: string): Lane {
    return this.lanes.find(lane => lane.id == id);
  }
  
  getIndex(lane: Lane): number {
    return this.lanes.findIndex(candidate => candidate == lane);
  }
  
  getPreviousId(id: string): Lane {
    let index = this.lanes.findIndex(lane => lane.id == id);
    return (index > 0 ? this.lanes[index - 1] : null);
  }
  
  getNextId(id: string): Lane {
    let index = this.lanes.findIndex(lane => lane.id == id);
    return (index < this.lanes.length - 1 ? this.lanes[index + 1] : null);
  }

}
